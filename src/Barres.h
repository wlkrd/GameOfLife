#include "pixel_operator.h"

class barres
{
	public:
	barres(GLuint texture1,GLuint texture2,GLuint texture3,GLuint texture4, unsigned int x1, unsigned int x2, unsigned int x3, unsigned int y1, unsigned int y2, unsigned int y3);

	void afficher(float &val, float pos_x, float pos_y);

	void fleches(int X,int Y,bool &move_cond,int &a,int &b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max, bool &cond_barre_horiz, bool &cond_barre_verti);
	void gauche(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max);
	void droite(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max);
	void haut(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max);
	void bas(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max);

	private:

	unsigned int x1,x2,x3,y1,y2,y3;

	GLuint texture1;
	GLuint texture2;
	GLuint texture3;
	GLuint texture4;
	unsigned int Y,X; 
};

void Projection(float val, float x, float y);

void Affichage(GLuint texture, float &val, int x1, int y1, int x2, int y2,int x3, int y3, int x4, int y4, float pos_x, float pos_y);
void Affichage_Droit(GLuint texture, float &val, int x1, int y1, int x2, int y2, float pos_x, float pos_y);

void Afficher_Grille(unsigned char* map, float pos_x, float pos_y,float val);

void Move(float &x, float &y, float dx, float dy, float val);