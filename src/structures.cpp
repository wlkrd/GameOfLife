#include "structures.h"

//breeder va vers la droite tire vers le haut
void Type1(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y,unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[i+dim_x*j]==0)
		{
			Bin_0(map,x+i,y+j);  
			setPixel( x+i,  y+j, 0);
		}
		else
		{
			Bin_1(map,x+i,y+j);  
			setPixel( x+i,  y+j, 255);
		}
	}			
}

//breeder va vers la droite tire vers le bas
void Type5(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[i+dim_x*j]==0)
		{
			Bin_0(map,x+i,y+dim_y-j);  
			setPixel( x+i,  y+dim_y-j, 0);
		}
		else
		{
			Bin_1(map,x+i,y+dim_y-j);  
			setPixel( x+i,  y+dim_y-j, 255);
		}			
	}	
}

//breeder va vers la gauche tire vers le haut
void Type7(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[i+dim_x*j]==0)
		{
			Bin_0(map,x+dim_x-i,y+j);  
			setPixel( x+dim_x-i,  y+j, 0);
		}
		else
		{
			Bin_1(map,x+dim_x-i,y+j);  
			setPixel( x+dim_x-i,  y+j, 255);
		}			
	}
}

//breeder va vers la gauche tire vers le bas
void Type3(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[i+dim_x*j]==0)
		{
			Bin_0(map,x+dim_x-i,y+dim_y-j);  
			setPixel( x+dim_x-i,  y+dim_y-j, 0);
		}
		else
		{
			Bin_1(map,x+dim_x-i,y+dim_y-j);  
			setPixel( x+dim_x-i,  y+dim_y-j, 255);
		}
	}			
}

//breeder va vers le bas tire vers la gauche
void Type6(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[j+dim_y*i]==0)
		{
			Bin_0(map,x+i,y+j);  
			setPixel( x+i,  y+j, 0);
		}
		else
		{
			Bin_1(map,x+i,y+j);  
			setPixel( x+i,  y+j, 255);
		}
	}			
}

//breeder va vers le bas tire vers la droite
void Type4(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[j+dim_y*i]==0)
		{
			Bin_0(map,x+dim_x-i,y+j);  
			setPixel( x+dim_x-i,  y+j, 0);
		}
		else 
		{
			Bin_1(map,x+dim_x-i,y+j);  
			setPixel( x+dim_x-i,  y+j, 255);
		}			
	}
}
//breeder va vers le haut tire vers la droite
void Type8(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[j+dim_y*i]==0)
		{
			Bin_0(map,x+dim_x-i,y+dim_y-j);  
			setPixel( x+dim_x-i,  y+dim_y-j, 0);
		}
		else
		{
			Bin_1(map,x+dim_x-i,y+dim_y-j);  
			setPixel( x+dim_x-i,  y+dim_y-j, 255);
		}
	}				
}

//breeder va vers le haut tire vers la gauche
void Type2(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y)
{
	unsigned int i,j;

	for(i=0;i<dim_x;i++)
	for(j=0;j<dim_y;j++)
	{
		if(DATA[j+dim_y*i]==0)
		{
			Bin_0(map,x+i,y+dim_y-j);  
			setPixel( x+i,  y+dim_y-j, 0);
		}
		else
		{
			Bin_1(map,x+i,y+dim_y-j);  
			setPixel( x+i,  y+dim_y-j, 255);
		}
	}				
}

//breeder : bouge en produisant des gosper glider canon
bool Lecteur_figure(unsigned char* DATA, char* name, unsigned int x_val, unsigned int y_val)
{  
    FILE* fichier2 = NULL;
	fopen_s(&fichier2,name, "r"); 

	if(fichier2!=NULL)
	{
		unsigned int x=0;
		unsigned  int y=0;
		char i;
		unsigned int j;
		unsigned char val;

		while(y<y_val)
		{
			while(x<x_val)
			{
				fscanf(fichier2,"%c ",&i);
				val=i-48;
				DATA[x+(y_val-1-y)*x_val]=val;
				x++;
			}
			fscanf(fichier2,"\n");
			x=0;
			y++;
		}

		fclose(fichier2); 
		return true;
	}
	else
	{
		return false;
	}
}

void Random(unsigned char *map, unsigned int xmin,unsigned int xmax,unsigned int ymin,unsigned int ymax)
{  
	unsigned int a,b;
			
	for (a =ymin ; a < ymax ; a=a+1)
	for (b = xmin; b < xmax ; b++)
	{
		BinChg(map,a,b,0 + (int) ((double) rand() * (2) / (RAND_MAX+1)));
		if(BinVal(map,a,b)==1){setPixel( a, b, 255);}
	}
}

void Orientation(int indice2, float pos_x, float pos_y, float val)
{

glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluOrtho2D(0, screensize+texture_menu+hauteur_barre, screensize+hauteur_barre, 0);
	glPointSize(1);

glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();

	FullBox(screensize+1+hauteur_barre, screensize+hauteur_barre+largeur_menu-2, 1, 148, 0,0,0);

	switch (indice2)//afficher les fl�ches en haut � droite
	{
		case 1:		fleche_D(screensize+hauteur_barre+75,screensize+hauteur_barre+128,75,255,204,204);		fleche_BD(screensize+hauteur_barre+75,75,35,168,230,29);		break;
		case 2:		fleche_H(screensize+hauteur_barre+75,21,74,255,204,204);								fleche_HD(screensize+hauteur_barre+75,75,35,168,230,29);		break;
		case 3:		fleche_G(screensize+hauteur_barre+21,screensize+hauteur_barre+74,75,255,204,204);		fleche_HG(screensize+hauteur_barre+40,40,35,168,230,29);		break;
		case 4:		fleche_B(screensize+hauteur_barre+75,75,128,255,204,204);								fleche_BG(screensize+hauteur_barre+40,109,35,168,230,29);		break;
		case 5:		fleche_D(screensize+hauteur_barre+75,screensize+hauteur_barre+128,75,255,204,204);		fleche_HD(screensize+hauteur_barre+75,75,35,168,230,29);		break;
		case 6:		fleche_B(screensize+hauteur_barre+75,75,128,255,204,204);								fleche_BD(screensize+hauteur_barre+75,75,35,168,230,29);		break;
		case 7:		fleche_G(screensize+hauteur_barre+21,screensize+hauteur_barre+74,75,255,204,204);		fleche_BG(screensize+hauteur_barre+40,109,35,168,230,29);		break;
		default:	fleche_H(screensize+hauteur_barre+75,21,74,255,204,204);								fleche_HG(screensize+hauteur_barre+40,40,35,168,230,29);		break;
	}

    // SDL_GL_SwapBuffers();
	//glCopyPixels (0,0,screensize+barre,screensize+barre,GL_COLOR);

	Projection(val, pos_x, pos_y);
}