#include "plan.h"
#include "bin_operator.h"

void Constr_de_plan( unsigned char *source,unsigned char *dest,int dim)
{
	unsigned int Dim=(unsigned int)dim;   
	unsigned int xd=0;
	unsigned int yd=0;
	char val8=0;
	unsigned int x=0;
	unsigned int y=0;

	bool continuer=true;

	while(continuer)
	{ 
		val8=val8|source[x+dim*y];         
		if(x<xd+7){x++;}
		else if (y<yd+7){x=xd;y++;}
		else
		{
			if(val8!=0)
			{
			dest[(xd>>3)+dim/8*(yd>>3)]=1;
			}
			else{dest[(xd>>3)+dim/8*(yd>>3)]=0;  }    
			val8=0;
			if(xd<=Dim-9){xd=xd+8;y=yd;x=x+1;} 
			else if(yd<=Dim-9){yd=yd+8;x=0;y=y+1;xd=0;}
			else{continuer=false;}
		}
	}       
}

void Constr_de_plan2( unsigned char *source,unsigned char *dest,int dim)
{
	unsigned int Dim=(unsigned int)dim;    
	unsigned int xd=0;
	unsigned int yd=0;
	char val8=0;
	unsigned int x=0;
	unsigned int y=0;
	bool continuer=true;

	while(continuer)
	{ 
		val8=val8|BinVal(source,x,y);     
		
		if(x<xd+7){x++;}
		else if (y<yd+7){x=xd;y++;}
		else
		{
			if(val8!=0)
			{
			dest[(xd>>3)+dim/8*(yd>>3)]=1;  
			}
			else{dest[(xd>>3)+dim/8*(yd>>3)]=0;  }
			val8=0;
			if(xd<=Dim-9){xd=xd+8;y=yd;x=x+1;} 
			else if(yd<=Dim-9){yd=yd+8;x=0;y=y+1;xd=0;}
			else{continuer=false;}
		}
	}    
}

void VIDER(unsigned int xmin,unsigned int xmax,unsigned int ymin,unsigned int ymax,unsigned char* map)
{
	unsigned int x,y;
 
	for(y=ymin;y<=ymax;y++)
	{
		for(x=(xmin>>3);x<=(xmax>>3);x++)
		{  
			 map[x+y*taille8]=0 ;                     
		}
	}     
}
