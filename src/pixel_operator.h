#include <stdlib.h> 
#include <stdio.h> 
#include <SDL\SDL.h> 
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#define screensize 1024
#define taille 4096//4096//taille 1024 et 2048 et 4096 marchent
#define taille8 (taille/8)
#define taille16 (taille/16)
#define taille64 (taille/64)
#define texture_menu 150 //taille de la texture ( 2^n )
#define largeur_menu 150 //taille de la partie utile de la texture PAS UTILIS2 POUR LE MOMENT
#define hauteur_barre 18//c'est la largeur aussi
#define texture_barre 32

#define mvt 10 //mouvement provoqu� par un clic sur les boutons de defilement
#define tps_mvt 100 //temps entre 2 mouvements de defilement

void setPixel(unsigned int x, unsigned int y, GLubyte red, GLubyte green, GLubyte blue);
void setPixel(unsigned int x, unsigned int y, Uint32 coul);
void setPixel10(unsigned int x, unsigned int y, GLubyte red, GLubyte green, GLubyte blue);
void FullBox(unsigned int x, unsigned int x2, unsigned int y, unsigned int y2, GLubyte red, GLubyte green, GLubyte blue);
void ligneV(unsigned int x, unsigned int y, unsigned int y2, GLubyte red, GLubyte green, GLubyte blue);
void ligneH(unsigned int x, unsigned int x2, unsigned int y, GLubyte red, GLubyte green, GLubyte blue);
void ligneD1(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue);//ligne haut gauche/bas  droite
void ligneD2(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue);//ligne  bas gauche/haut droite

void fleche_D(unsigned int x, unsigned int x2, unsigned int y, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers la droite
void fleche_G(unsigned int x, unsigned int x2, unsigned int y, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers la gauche
void fleche_B(unsigned int x, unsigned int y, unsigned int y2, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers le bas
void fleche_H(unsigned int x, unsigned int y, unsigned int y2, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers le haut
void fleche_HD(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers en haut � droite
void fleche_BD(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers en bas � droite
void fleche_HG(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers en haut � gauche
void fleche_BG(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue);//fl�che vers en bas � gauche
