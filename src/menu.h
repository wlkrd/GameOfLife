#include "bouton.h"

class menu
{
	public:
	menu(char* text, unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);
	void afficher(float &val, float pos_x, float pos_y, int indice,unsigned char *tableau);
	void affichage_primaire();

	private:
	GLuint texture;
	unsigned int xmin;
	unsigned int xmax;
	unsigned int ymin;
	unsigned int ymax;
};

void Fonctions_Menu(bouton *tab[nbre_boutons], unsigned int x, unsigned int y,int &indice,int &indice2, int &mode,bool &continuer, float pos_x, float pos_y, float val,int &indice3);
void Load(unsigned char* map, unsigned char* map2, unsigned char* p, unsigned char* q, unsigned char* r, unsigned char* s, float pos_x, float pos_y,float val,int indice2,menu &Menu,barres &Barres, unsigned char *tableau);
void Save(unsigned char* map);

void Deplacer_Barre_horizontale(barres &Barre, menu &Menu,unsigned int X, float val, float &pos_x, float &pos_y,int indice2, unsigned char* tableau, unsigned char *map);
void Deplacer_Barre_verticale(barres &Barre, menu &Menu,unsigned int Y, float val, float &pos_x, float &pos_y,int indice2, unsigned char* tableau, unsigned char *map);
void changer_etat(unsigned char tab[15*15], unsigned int x, unsigned int y);
void reset(unsigned char tab[15*15]);



