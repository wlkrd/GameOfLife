#include "schema.h"

schema::schema(unsigned char *tab, unsigned int x, unsigned int y, char* name, bool &DEFINE)
{
	this->tab = tab;
	this->x = x;
	this->y = y;
	if(name != NULL)
	{
		DEFINE = (DEFINE && Lecteur_figure(tab,name,x,y));
	}
}

unsigned int schema::get_x()
{
	return x;
}

unsigned int schema::get_y()
{
	return y;
}

void schema::Create_Struct(unsigned char *map,unsigned int x,unsigned int y,unsigned char *plan,unsigned char *plan2,int orientation)
{
	unsigned int i,j;
	unsigned int dim_x; 
	unsigned int dim_y;

	if(orientation % 2 ==1)
	{
		dim_x=this->x; 
		dim_y=this->y;
	}
	else
	{
		dim_x=this->y; 
		dim_y=this->x;
	}

	for(i=(x>>3);i<=((x+dim_x)>>3);i++)
	for(j=(y>>3);j<=((y+dim_y)>>3);j++)
	{
		plan[i+j*taille8]=1;
	}

	for(i=(x>>6);i<=((x+dim_x)>>6);i++)
	for(j=(y>>6);j<=((y+dim_y)>>6);j++)
	{
		plan2[i+j*taille64]=1;
	}

	switch (orientation)
	{
		case 1:		Type1(tab,dim_x,dim_y,map,x,y);		break;
		case 2:		Type2(tab,dim_x,dim_y,map,x,y);		break;
		case 3:		Type3(tab,dim_x,dim_y,map,x,y);		break;
		case 4:		Type4(tab,dim_x,dim_y,map,x,y);		break;
		case 5:		Type5(tab,dim_x,dim_y,map,x,y);		break;
		case 6:		Type6(tab,dim_x,dim_y,map,x,y);		break;
		case 7:		Type7(tab,dim_x,dim_y,map,x,y);		break;
		default:	Type8(tab,dim_x,dim_y,map,x,y);		break;
	}
}
