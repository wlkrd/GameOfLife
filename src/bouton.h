#include "schema.h"

#define hauteur_bouton 24  //boutons pause et autres
#define hauteur_bouton2 24 //boutons de glider, vaisseaux, etc
#define hauteur_bouton3 72 //bouton breeder
#define largeur_bouton3 72//bouton breeder
#define largeur_bouton 24
#define x_bouton (screensize+hauteur_barre)

#define nbre_boutons 22

#define x_1 x_bouton+10//pause
#define x_2 (x_1+largeur_bouton+10)//199 slow
#define x_3 (x_2+largeur_bouton+10)//245 normal
#define x_4 (x_3+largeur_bouton+10)//291 fast
#define x_5 (x_bouton+34)//337 miroir
#define x_6 (x_5+largeur_bouton+34)//383 turn
#define x_7 x_bouton+2//breeder_av
#define x_8 x_bouton//594 glider
#define x_9 (x_8+largeur_bouton+2)//629 gg gun
#define x_10 (x_9+largeur_bouton+2)//664 ship
#define x_11 (x_10+largeur_bouton+2)//699 exploder
#define x_12 (x_11+largeur_bouton+2)//734 unknown
#define x_13 x_bouton//769 f-pentamino
#define x_14 (x_bouton+largeur_bouton+2)//804 acorn
#define x_15 (x_bouton+19)//839 void
#define x_16 (x_15+largeur_bouton+19)// sauvegarde
#define x_17 (x_16+largeur_bouton+19)//// charger
#define x_18 (x_7+largeur_bouton3+2)// breeder_ar
#define x_19 (x_bouton+34)//blit
#define x_20 (x_19+largeur_bouton+34)//vider
#define x_21 (x_bouton+2)//l_gun
#define x_22 (x_21+largeur_bouton3+2)//puffer

#define y_1 173 //pause
#define y_2 y_1//199 slow
#define y_3 y_1//245 normal
#define y_4 y_1//291 fast
#define y_5 (y_4+hauteur_bouton+20)//337 miroir
#define y_6 y_5//383 turn
#define y_7 453 //breeder_av
#define y_8 (y_7+hauteur_bouton3+4)//594 glider
#define y_9 y_8//629 gg gun
#define y_10 y_9//664 ship
#define y_11 y_10//699 exploder
#define y_12 y_11//734 unknown
#define y_13 (y_12+hauteur_bouton2+4)//769 f-pentamino
#define y_14 y_13//804 acorn
#define y_15 (y_5+hauteur_bouton+20)//839 void
#define y_16 y_15//sauvegarde
#define y_17 y_15//charger
#define y_18 y_7//breeder_ar
#define y_19  (890-hauteur_bouton2-4)//blit
#define y_20  y_19//vider
#define y_21  (y_7- hauteur_bouton3-2)//l_gun
#define y_22  y_21//puffer

class bouton
{
	public:

	bouton(unsigned int x_min,unsigned int x_max,unsigned int y_min,unsigned int y_max,unsigned int valeur,char *string_texture);
	void affichage(float &val, float pos_x, float pos_y);
	bool appartient(unsigned int x, unsigned int y);
	void fonction(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);


	private:

	unsigned int x_min;
	unsigned int x_max;
	unsigned int y_min;  
	unsigned int y_max;  
	unsigned int valeur;
	//fun(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val);
	GLuint texture;
};

void f0(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f1(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f2(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f3(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f4(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f5(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f6(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f7(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f8(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f9(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f10(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f11(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f12(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f13(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f14(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f15(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f16(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f17(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f18(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f19(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f20(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);
void f21(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3);

GLuint loadTexture(const char * filename,bool useMipMap = true);
SDL_Surface * flipSurface(SDL_Surface * surface);