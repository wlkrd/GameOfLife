# GameOfLife

<br />

This program is an implementation of Conway's Game of Life.

<br />
<img src="https://gitlab.com/FrVi/GameOfLife/raw/49b38a2f8bcc8a0e9e0fd3739551ff7e5eaa7b53/exe/data/Jeu%20de%20la%20vie.png" alt="Drawing"/>
<br />

<br />

Rules are pretty simple: 
- A cell stays alive only if it has **two** or **three** neighbors.
- An empty cell becomes alive if it has **three** neighbors.
- In all configurations, the cell becomes or stays empty.

<br />
Neighbors of a cell are its 8 surrounding cells (left, right, up, bottom, left-up, right-bottom...).

The rules seams really simple but they allow interesting patterns like small "ships" travelling in straight lines and "cannons".

In fact it is even possible to build a Turing machine with those rules!

<br />
## Commands

You can **left-click** in the screen to add an object.

The **mouse wheel** controls the zoom.

You can use the **right menu** to change the pattern you draw, pause, increase/reduce speed and change orientation of patterns.

To **un-pause**, click the pause button again (the "play" symbol resets the speed to normal speed).

The square in the lower right corner allow you to design a pattern pixels by pixels and use the two buttons to erase or select it for draw.

<br />
This version is single threaded.

<br />
## Idea

While I finally did not implement it, I had the idea to implement a two colors version:
- The survival/birth rule stays the same as above.
- A cell get the color of the majority of its neighbors.

<br />
It is also possible to set more complicated rules like:
- A live cell stays of its color if it has only two neighbors.
- It changes color if it has three neighbors with at least two of the other color.


